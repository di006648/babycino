class TestBugI1 {
	public static void main(String[] a) {
		System.out.println(new Test().f());
	}
}

class Test{
	public int f() {
		int result;
		// create boolean x
		boolean x;
		x = true;
		// attempt to increment boolean value 
		x++; 
		if (x) {
			result = 0;
		} else {
			result = 1;
		}
		return result;
	}
}